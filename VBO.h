#ifndef VBO_H
#define VBO_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glm/glm.hpp>
#include <glad/glad.h>
#include <vector>

//This struct is a holder for a set variable to use in the mesh class
struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textUV;
};

class VBO 
{
public:

	GLuint ID;
	VBO(std::vector<Vertex>& vertices);

	void bind();
	void unbind();
	void destroy();
};

#endif // !VBO_H