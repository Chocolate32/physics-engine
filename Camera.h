#ifndef CAMERA_H
#define CAMERA_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glad/glad.h>
#include<GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include<glm/gtx/rotate_vector.hpp>
#include<glm/gtx/vector_angle.hpp>

#include "Shader.h"

class Camera
{
public:
	Camera(glm::vec3 position, unsigned int width, unsigned int height);
	void scrolling(float yoffset);
	void matrix(float nPlane, float fPlane, Shader& shader, const char* uniform);
	void processCamInputs(GLFWwindow* window, float deltatime);
	glm::vec3 currentPosition();
	glm::vec3 frontDirection();

private:
	bool firstClick = true;
	float speed = 2.5f;
	float sensitivity = 100.0f;
	float fov=45.0f;
	unsigned int width, height;

	glm::vec3 position;
	glm::vec3 orientation = glm::vec3(0.0, 0.0, -1.0);
	glm::vec3 right = glm::vec3();
	glm::vec3 up = glm::vec3(0.0, 1.0, 0.0);
};

#endif // !CAMERA_H


