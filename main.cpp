//#include <stdio.h>
//#include <math.h>
//#include <glad/glad.h>
//#include <GLFW/glfw3.h>
//
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
//
//#include "Shader.h"
//#include "VAO.h"
//#include "VBO.h"
//#include "EBO.h"
#include "Texture.h"
#include "Camera.h"
#include "Mesh.h"


#pragma region Prototypes
//callbacks prototypes
void framebuffer_resize_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

//functions prototypes
void processInput(GLFWwindow* window);

//screen Resolution definition
const unsigned int SCREEN_WIDTH = 800, SCREEN_HEIGHT = 800;
#pragma endregion

#pragma region Camera_Variables
//camera transform vector
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);

//initialize a camera class
Camera camera(cameraPos, SCREEN_WIDTH, SCREEN_HEIGHT);

//camera configuration
bool firstMouse = true;
bool firstPress = true;
float lastX = SCREEN_WIDTH / 2;
float lastY = SCREEN_HEIGHT / 2;

//frame time
float deltaTime = 0.0f;
float lastFrame = 0.0f;
#pragma endregion

glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

int nbr_of_pointLights = 4;

int main() {

#pragma region Init
    //initialize glfw
    if (!glfwInit()) {
        printf("Error while initializing glfw!!");
        glfwTerminate();
        return 1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    //initialize window frame
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "PhysicsEngine", NULL, NULL);
    if (!window) {
        printf("Failed to create a window frame");
        glfwTerminate();
        return -1;
    }

    //link frame context
    glfwMakeContextCurrent(window);

    //register callbacks
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);
    glfwSetScrollCallback(window, scroll_callback);


    //initialize glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialise glad!");
        glfwDestroyWindow(window);
        glfwTerminate();
        return 1;
    }

    //stbi_set_flip_vertically_on_load(true);

    //by enabling the depth buffer our mech information doesn't collapse 
    glEnable(GL_DEPTH_TEST);

#pragma endregion

    //initialize a shader program class
    Shader textureProgram("combinedLightTextShader.vert", "combinedLightTextShader.frag");
    Shader lightProgram("lightShader.vert","lightShader.frag");


    //this is responsible for drawing a rectangle(two triangles) if 1 triangle only 3 variables needed, if 2 then 6 needed,
    //note that you can forme a rectangle with 2 triangles!!

    //float yAxes = sqrt(12.0f) / 10;

    //GLfloat texCordinates[] = {
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    0.5f, 1.0f
    //};

#pragma region Old_vertices
    //GLfloat vertices[] = {
    //    ////rectangle cordinates
    //    //0.5f, 0.5f, 0.0f,
    //    //0.5f, -0.5f, 0.0f,
    //    //-0.5f, -0.5f, 0.0f,
    //    //-0.5f, 0.5f, 0.0f,

    //    //  //positions          // colors           // texture coords
    //    // 0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,
    //    // 0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,
    //    //-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
    //    //-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f

    //    // // positions          // texture coords
    //    // 0.5f,  0.5f, 0.0f,   1.0f, 1.0f,
    //    // 0.5f, -0.5f, 0.0f,   1.0f, 0.0f,
    //    //-0.5f, -0.5f, 0.0f,   0.0f, 0.0f,
    //    //-0.5f,  0.5f, 0.0f,   0.0f, 1.0f

    //    //Cube cordinates     //texture cords   //normal coords
    //    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,       0.0f,  0.0f, -1.0f,
    //     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,       0.0f,  0.0f, -1.0f,
    //     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,       0.0f,  0.0f, -1.0f,
    //     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,       0.0f,  0.0f, -1.0f,
    //    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,       0.0f,  0.0f, -1.0f,
    //    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,       0.0f,  0.0f, -1.0f,

    //    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,       0.0f,  0.0f,  1.0f,
    //     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,       0.0f,  0.0f,  1.0f,
    //     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,       0.0f,  0.0f,  1.0f,
    //     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,       0.0f,  0.0f,  1.0f,
    //    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,       0.0f,  0.0f,  1.0f,
    //    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,       0.0f,  0.0f,  1.0f,

    //    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       -1.0f,  0.0f,  0.0f,
    //    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,       -1.0f,  0.0f,  0.0f,
    //    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       -1.0f,  0.0f,  0.0f,
    //    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       -1.0f,  0.0f,  0.0f,
    //    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,       -1.0f,  0.0f,  0.0f,
    //    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       -1.0f,  0.0f,  0.0f,

    //     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       1.0f,  0.0f,  0.0f,
    //     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,       1.0f,  0.0f,  0.0f,
    //     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       1.0f,  0.0f,  0.0f,
    //     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       1.0f,  0.0f,  0.0f,
    //     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,       1.0f,  0.0f,  0.0f,
    //     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       1.0f,  0.0f,  0.0f,

    //    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       0.0f, -1.0f,  0.0f,
    //     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,       0.0f, -1.0f,  0.0f,
    //     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,       0.0f, -1.0f,  0.0f,
    //     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,       0.0f, -1.0f,  0.0f,
    //    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,       0.0f, -1.0f,  0.0f,
    //    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,       0.0f, -1.0f,  0.0f,

    //    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,       0.0f,  1.0f,  0.0f,
    //     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,       0.0f,  1.0f,  0.0f,
    //     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       0.0f,  1.0f,  0.0f,
    //     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,       0.0f,  1.0f,  0.0f,
    //    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,       0.0f,  1.0f,  0.0f,
    //    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,       0.0f,  1.0f,  0.0f

    //    ////3 triangles cordinates    //color
    //    //-0.2f, 0.0f, 0.0f,          1.0f, 1.0f, 0.0f,
    //    //0.2f, 0.0f, 0.0f,           1.0f, 0.0f, 1.0f,
    //    //0.0f, yAxes, 0.0f,          1.0f, 0.0f, 0.0f,

    //    //-0.4f, -yAxes, 0.0f,        0.0f, 1.0f, 0.0f,
    //    //0.0f, -yAxes, 0.0f,         0.0f, 1.0f, 1.0f,
    //    //-0.2f, 0.0f, 0.0f,          1.0f, 1.0f, 0.0f,

    //    //0.0f, -yAxes, 0.0f,         0.0f, 1.0f, 1.0f,
    //    //0.4f, -yAxes, 0.0f,         0.0f, 0.0f, 1.0f,
    //    //0.2f, 0.0f, 0.0f,           1.0f, 0.0f, 1.0f,

    //    ////3 triangles cordinates    //color             //texture coordinates
    //    //-0.2f, 0.0f, 0.0f,          1.0f, 1.0f, 0.0f,   0.0f, 0.0f,
    //    //0.2f, 0.0f, 0.0f,           1.0f, 0.0f, 1.0f,   1.0f, 0.0f,
    //    //0.0f, yAxes, 0.0f,          1.0f, 0.0f, 0.0f,   0.5f, 1.0f,

    //    //-0.4f, -yAxes, 0.0f,        0.0f, 1.0f, 0.0f,   0.0f, 0.0f,
    //    //0.0f, -yAxes, 0.0f,         0.0f, 1.0f, 1.0f,   1.0f, 0.0f,
    //    //-0.2f, 0.0f, 0.0f,          1.0f, 1.0f, 0.0f,   0.5f, 1.0f,

    //    //0.0f, -yAxes, 0.0f,         0.0f, 1.0f, 1.0f,   0.0f, 0.0f,
    //    //0.4f, -yAxes, 0.0f,         0.0f, 0.0f, 1.0f,   1.0f, 0.0f,
    //    //0.2f, 0.0f, 0.0f,           1.0f, 0.0f, 1.0f,   0.5f, 1.0f,

    //    ////3 triangles cordinates
    //    //-0.2f, 0.0f, 0.0f,
    //    //0.2f, 0.0f, 0.0f,
    //    //0.0f, yAxes, 0.0f,

    //    //-0.4f, -yAxes, 0.0f,
    //    //0.0f, -yAxes, 0.0f,
    //    //-0.2f, 0.0f, 0.0f,

    //    //0.0f, -yAxes, 0.0f,
    //    //0.4f, -yAxes, 0.0f,
    //    //0.2f, 0.0f, 0.0f,

    //    ////1 triangle
    //    //0.0f, 0.5f, 0.0f,
    //    //-0.5f, -0.5f, 0.0f,
    //    //0.5f, -0.5f, 0.0f
    //};
#pragma endregion

std::vector<Vertex> vertices = {
    //              Coordinates                 //Normals                       //TextCoords
    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(0.5f, -0.5f, -0.5f),   glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(0.5f,  0.5f, -0.5f),   glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(0.5f,  0.5f, -0.5f),   glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(0.0f,  0.0f, -1.0f),  glm::vec2(0.0f, 0.0f)},
    
    Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(0.5f, -0.5f,  0.5f),   glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec3(0.0f,  0.0f,  1.0f),  glm::vec2(0.0f, 0.0f)},

    Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(1.0f, 0.0f)},

    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(0.5f,  0.5f, -0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(0.5f, -0.5f, -0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(0.5f, -0.5f, -0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(0.5f, -0.5f,  0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(1.0f,  0.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},

    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(0.5f, -0.5f, -0.5f),   glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(0.5f, -0.5f,  0.5f),   glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(0.5f, -0.5f,  0.5f),   glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec3(0.0f, -1.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},

    Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3(0.5f,  0.5f, -0.5f),   glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(0.5f,  0.5f,  0.5f),   glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(1.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec3(0.0f,  1.0f,  0.0f),  glm::vec2(0.0f, 1.0f)},
};

    //this is responsible for the indicices to draw two triangles as a rectangle
    std::vector<GLuint> indices = {
        ////Rectangle indicies
        //0,1,2,
        //3,4,5

        ////3 triangle indicies
        //0,1,2,
        //3,4,1,
        //5,4,1
    };
    for (unsigned int i = 0; i < 36; ++i)
        indices.push_back(i);

    //spawn more cubes
    glm::vec3 cubePositions[] = {
    glm::vec3(0.0f,  0.0f,  0.0f),
    glm::vec3(2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3(2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3(1.3f, -2.0f, -2.5f),
    glm::vec3(1.5f,  2.0f, -2.5f),
    glm::vec3(1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f)
    };

    //position of all point lights
    glm::vec3 pointLightPositions[] = {
    glm::vec3(0.7f,  0.2f,  2.0f),
    glm::vec3(2.3f,  -3.3f, -4.0f),
    glm::vec3(-4.0f, 2.0f, -12.0f),
    glm::vec3(0.0f, 0.0f, -3.0f)
    };

    //we use vertex array object (VBO) to make one triangle or 2...,
    //we use element array object (EBO) to draw a rectangle
    //the VAO is responsible of carrying both of them to the GPU because shaders are draws using the graphics

    ////graphical models creation
    //VAO cubeVAO,lightVAO;
    //cubeVAO.bind();

    //VBO VBO(vertices);

    //EBO EBO(indices);

    //cubeVAO.linkVBO(VBO, 0, 3, GL_FLOAT, 8 * sizeof(float), (void*)0);
    //cubeVAO.linkVBO(VBO, 1, 2, GL_FLOAT, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    //cubeVAO.linkVBO(VBO, 2, 3, GL_FLOAT, 8 * sizeof(float), (void*)(5 * sizeof(float)));
    //cubeVAO.unbind();
    //VBO.unbind();
    //EBO.unbind();
    std::vector<Texture> textures = {
        Texture("container2.png", "diffuse", GL_TEXTURE0, GL_RGBA, GL_UNSIGNED_BYTE),
        Texture("container2_specular.png", "specular", GL_TEXTURE1, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    
    Mesh crate(vertices, indices, textures);
    Mesh light(vertices, indices, textures);

    //textureProgram.use();

    //texture1.texUnit(textureProgram, "material.diffuse", 0);
    //texture2.texUnit(textureProgram, "material.specular", 1);

    //texture1.unbind();
    //texture2.unbind();

    ////light creation
    //lightVAO.bind();
    //lightVAO.linkVBO(VBO, 0, 3, GL_FLOAT, 8 * sizeof(float), (void*)0);
    //



    ////messing around with the postion
    //GLuint  transFromLoc = myShader.getUniformID("transform");
    //glUniformMatrix4fv(transFromLoc, 1, GL_FALSE, glm::value_ptr(trans));
    
    //glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
    //glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
    //glm::vec3 cameraDirection = glm::normalize(cameraPos - cameraTarget);

    //glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    //glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));

    //glm::vec3 cameraUp = glm::cross(cameraDirection, cameraRight);



    const float radius = 10.0f;

    //this is a way to draw in wireframe mode
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //render loop
    while (!glfwWindowShouldClose(window)) {
        double currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        //manage inputs
        processInput(window);

        camera.processCamInputs(window, deltaTime);
        //rendering commands
        glClearColor(0.1f, 0.1f, 0.1f, 0.5f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        lightProgram.use();
        //lightVAO.bind();


        glm::vec3 lightColor(1.0f, 1.0f, 1.0f);
        glUniform3fv(lightProgram.getUniformID("viewPos"), 1, glm::value_ptr(camera.currentPosition()));
        glUniform3fv(lightProgram.getUniformID("objectColor"), 1, glm::value_ptr(lightColor));

        camera.matrix(0.1f, 100.0f, lightProgram, "camMatrix");
        glm::mat4 model;
        int modelLoc = lightProgram.getUniformID("model");
        for (int i = 0; i < nbr_of_pointLights; ++i) {
            model = glm::mat4(1.0f);
            //model = glm::rotate(model, (float)currentFrame * glm::radians(50.0f), glm::vec3(0.0, 1.0, 0.0));
            model = glm::translate(model, pointLightPositions[i]);
            glm::vec4 currentLightPos = model * glm::vec4(1.0);
            model = glm::scale(model, glm::vec3(0.2f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

            light.Draw(lightProgram, camera);

            //glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        //texture1.bind();
        //texture2.bind();

        //this will start the drawing to the binded VAO
        textureProgram.use();
        
        glUniform3fv(textureProgram.getUniformID("viewPos"), 1, glm::value_ptr(camera.currentPosition()));
        glUniform1f(textureProgram.getUniformID("material.shininess"), 16.0f);
        camera.matrix(0.1f, 100.0f, textureProgram, "camMatrix");

        //directional light
        glUniform3fv(textureProgram.getUniformID("dirLight.direction"), 1, glm::value_ptr(glm::vec3(-0.2f, -1.0f, -0.3f)));
        glUniform3fv(textureProgram.getUniformID("dirLight.ambient"), 1, glm::value_ptr(lightColor * glm::vec3(0.05f)));
        glUniform3fv(textureProgram.getUniformID("dirLight.diffuse"), 1, glm::value_ptr(lightColor * glm::vec3(0.4f)));
        glUniform3fv(textureProgram.getUniformID("dirLight.specular"), 1, glm::value_ptr(lightColor * glm::vec3(0.5f)));

        //point lights
        for (int i = 0; i < nbr_of_pointLights; ++i) {
            std::string name = "pointLight[" + std::to_string(i) + ']';
            glUniform3fv(textureProgram.getUniformID((name + ".position").c_str()), 1, glm::value_ptr(pointLightPositions[i]));
            glUniform3fv(textureProgram.getUniformID((name + ".ambient").c_str()), 1, glm::value_ptr(lightColor * glm::vec3(0.05f)));
            glUniform3fv(textureProgram.getUniformID((name + ".diffuse").c_str()), 1, glm::value_ptr(lightColor * glm::vec3(0.8f)));
            glUniform3fv(textureProgram.getUniformID((name + ".specular").c_str()), 1, glm::value_ptr(lightColor * glm::vec3(1.0f)));
            glUniform1f(textureProgram.getUniformID((name + ".constant").c_str()), 1.0f);
            glUniform1f(textureProgram.getUniformID((name + ".linear").c_str()), 0.09f);
            glUniform1f(textureProgram.getUniformID((name + ".quadratic").c_str()), 0.032f);
        }

        //spot light
        glUniform3fv(textureProgram.getUniformID("spotLight.position"), 1, glm::value_ptr(camera.currentPosition()));
        glUniform3fv(textureProgram.getUniformID("spotLight.direction"), 1, glm::value_ptr(camera.frontDirection()));
        glUniform3fv(textureProgram.getUniformID("spotLight.ambient"), 1, glm::value_ptr(lightColor * glm::vec3(0.0f)));
        glUniform3fv(textureProgram.getUniformID("spotLight.diffuse"), 1, glm::value_ptr(lightColor * glm::vec3(1.0f)));
        glUniform3fv(textureProgram.getUniformID("spotLight.specular"), 1, glm::value_ptr(lightColor * glm::vec3(1.0f)));
        glUniform1f(textureProgram.getUniformID("spotLight.constant"), 1.0f);
        glUniform1f(textureProgram.getUniformID("spotLight.linear"), 0.09f);
        glUniform1f(textureProgram.getUniformID("spotLight.quadratic"), 0.032f);
        glUniform1f(textureProgram.getUniformID("spotLight.cutOff"), glm::cos(glm::radians(9.5f)));
        glUniform1f(textureProgram.getUniformID("spotLight.outterCutOff"), glm::cos(glm::radians(12.5f)));

        //material properties

        //glm::mat4 view;
        //glm::mat4 proj = glm::mat4(1.0f);

        //// note that we're translating the scene in the reverse direction of where we want to move
        //view = camera.lookingAt();

        //// this is prespective projection with 45 angle
        //proj = glm::perspective(glm::radians((float)camera.fov), float(SCREEN_WIDTH) / SCREEN_HEIGHT, 0.1f, 100.0f);

        //int viewLoc = myShader.getUniformID("view");
        //glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        //int projLoc = myShader.getUniformID("proj");
        //glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));
        modelLoc = textureProgram.getUniformID("model");

        ////control the color green over time
        //float timeValue = glfwGetTime();
        //float greenValue = (sin(timeValue) / 2.0f) + 0.5f;
        //int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
        //glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);

        ////this will allow us to draw a triangle(s)
        //glDrawArrays(GL_TRIANGLES, 0, 3);
        //glDrawArrays(GL_TRIANGLES, 3, 3);
        //glDrawArrays(GL_TRIANGLES, 6, 3);

        //cubeVAO.bind();
        ////this will allow us to draw trianges as quads
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        //glDrawArrays(GL_TRIANGLES, 0, 36);

        for (unsigned int i = 0; i < 10; ++i) {
            model = glm::mat4(1.0f);
            model = glm::translate(model, cubePositions[i]);
            float angle = 20.f * i;
            ////make the cubes rotate
            //model = glm::rotate(model, (float)currentFrame * glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
            model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

            crate.Draw(textureProgram, camera);

            //glDrawArrays(GL_TRIANGLES, 0, 36);
        }
        ////some animation massing around
        //float sintrans = sin((float)glfwGetTime());
        //glUniformMatrix4fv(transFromLoc, 1, GL_FALSE, glm::value_ptr(trans));
        //trans = glm::mat4(1.0f);
        //trans = glm::translate(trans, glm::vec3(0.0f, sintrans/2 - 0.5f, 0.0f));
        //trans = glm::scale(trans, glm::vec3(sintrans>0.25f?sintrans:0.25f , sintrans > 0.25f ? sintrans : 0.25f, 1.0f));
        //trans = glm::rotate(trans, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));

        //this is for 3d

        //check for registered events and swap the buffers
        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    //de-allocating all the recources before exiting
    //cubeVAO.destroy();
    //lightVAO.destroy();
    //EBO.destroy();
    //VBO.destroy();
    //texture1.destroy();
    //texture2.destroy();
    textureProgram.destroy();
    lightProgram.destroy();

    //making sure we clear the stack before closing the program
    glfwDestroyWindow(window);
    glfwTerminate();
	return 0;
}

//resize window call back buffer
void framebuffer_resize_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

//input managment 
void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

//mouse movement call back auto registered
void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;
}

//scroll call back auto registered
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    camera.scrolling((float)yoffset);
}