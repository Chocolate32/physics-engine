#include "VBO.h"

//constructor of the VBO will generate buffer and bind it to send data later on
VBO::VBO(std::vector<Vertex>& vertices)
{
	glGenBuffers(1, &ID);
	glBindBuffer(GL_ARRAY_BUFFER, ID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
}

//bind the data
void VBO::bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, ID);
}

//unbind the data
void VBO::unbind() 
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

//destroy the object buffer
void VBO::destroy()
{
	glDeleteBuffers(1, &ID);
}