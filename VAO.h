#ifndef VAO_H
#define VAO_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glad/glad.h>
#include "VBO.h"

class VAO
{
public:
	GLuint ID;
	VAO();

	void linkVBO(VBO& VBO, GLuint layout, GLuint nbComponents, GLenum type, GLsizeiptr stride, void* offset);
	void bind();
	void unbind();
	void destroy();
};

#endif // !VAO_H