#ifndef SHADER_H
#define SHADER_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glad/glad.h>

#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>
#include <cerrno>

std::string get_file_contents(const char* filename);

class Shader
{
public:
    unsigned int ID;

    Shader(const char* vertexFile, const char* fragmentFile);

    void use();

    void destroy();

    GLuint getUniformID(const char* a);

private:
    void errorHandler(unsigned int shader, const char* type);
};

#endif
