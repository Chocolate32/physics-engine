#ifndef EBO_H
#define EBO_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glad/glad.h>
#include <vector>

class EBO
{
public:
	GLuint ID;
	EBO(std::vector<GLuint>& indices);

	void bind();
	void unbind();
	void destroy();
};

#endif // !EBO_h