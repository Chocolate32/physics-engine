#include "Texture.h"

//This is the Texture constructor, it will take as input a path the image, a texture type (specular/diffuse) it's adapted to the mesh,
//a format of image to know the color vectors to use and finaly a pixel type for encoding purposes
Texture::Texture(const char* image, const char* textType, GLenum slot, GLenum format, GLenum pixelType)
{
    // Assigns the type of the texture to the texture object
	type = textType;

    //variables for storing image texture data information
    int width, height, nrChannels;
    //this will swap the way stbi reads the coordinates 
    stbi_set_flip_vertically_on_load(true);

    //generates a texture object
    glGenTextures(1, &ID);
    //assign the texture and bind it
    glActiveTexture(slot);
    glBindTexture(GL_TEXTURE_2D, ID);

    // set the texture wrapping/filtering options (on the currently bound texture object)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    //readding texture data
    unsigned char* data = stbi_load(image, &width, &height, &nrChannels, 0);
    ////in case of using GL_CLAMP_TO_BORDER we use those following
    //float flatColor[] = { 1.0f,1.0f,1.0f,1.0f };
    //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, flatColor);

    // load and generate the texture

    //check for errors
    if (data) {
        //assign the image to the texture object
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, pixelType, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
        printf("Failed to load the image texture1");

    //free data then unbinding
    stbi_image_free(data);
    //glBindTexture(textType, 0);
}

//update the uniform in the graphics card using this function
void Texture::texUnit(Shader& shader, const char* uniform, GLuint unint)
{
    // Gets the location of the uniform
    GLuint texUni = glGetUniformLocation(shader.ID, uniform);
    // Shader needs to be activated before changing the value of a uniform
    shader.use();
    // Sets the value of the uniform
    glUniform1i(texUni, unint);
}

//bind the texture to object
void Texture::bind()
{
    glBindTexture(GL_TEXTURE_2D, ID);
}

//unbind the texture
void Texture::unbind()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}

//free memory
void Texture::destroy()
{
    glDeleteTextures(1, &ID);
}