#include "Shader.h"

//this function will allow us to open, read then close a file, because a shader program exists in a seperate file
std::string get_file_contents(const char* filename) 
{
    std::ifstream in(filename, std::ios::binary);
    if (in) {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return(contents);
    }
    throw(errno);
}

//this is the Shader constructor, it will take a path to the vertex file and fragment file as inputs to compile for later uses
Shader::Shader(const char* vertexFile, const char* fragmentFile)
{
    //first step is to actually open the files to retrive the program
    std::string vertexCode = get_file_contents(vertexFile);
    std::string fragmentCode = get_file_contents(fragmentFile);

    //this a type convertion because it must be a pointer to char
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    //initializing an ID for both fragment and vertex, then we want to keep track if operations succeds
    GLuint vertex, fragment;

    //creating and compiling the vertex shader program first
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);

    //checks if anything goes wrong
    errorHandler(vertex, "VERTEX");

    //creating and compitiong the fragment shader program
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);

    //checks if anything goes wrong
    errorHandler(fragment, "FRAGMENT");

    //finally we link the vertex and fragment shader together
    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);

    //checks if anything goes wrong
    errorHandler(ID, "PROGRAM");

    //we don't need those se it's better to delete them from gpu
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

//a call to use the program
void Shader::use()
{
    glUseProgram(ID);
}

//destroy the program once we're done using
void Shader::destroy() 
{
    glDeleteProgram(ID);
}

//return a uniform location in the graphics card so we can modify it
GLuint Shader::getUniformID(const char* a) {
    return glGetUniformLocation(ID, a);
}

//this for error managment
void Shader::errorHandler(unsigned int shader, const char* type) {
    GLint success;
    char infoLog[512];

    if (type != "PROGRAM") {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(shader, 512, NULL, infoLog);
            printf("ERROR - SHADER %s COMPILATION_FAILED == %s", type, infoLog);
        }
    }
    else {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(shader, 512, NULL, infoLog);
            printf("ERROR - SHADER %s COMPILATION_FAILED == %s", type, infoLog);
        }
    }
}