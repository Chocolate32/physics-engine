#ifndef TEXTURE_H
#define TEXTURE_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <glad/glad.h>
#include <stb/stb_image.h>

#include "Shader.h"


class Texture
{
public:
	GLuint ID;
	const char* type;

	Texture(const char* image, const char* textType, GLenum slot, GLenum format, GLenum pixelType);

	void texUnit(Shader& shader, const char* uniform, GLuint unint);
	void bind();
	void unbind();
	void destroy();

};


#endif // !TEXTURE_H