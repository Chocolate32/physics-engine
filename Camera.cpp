#include "Camera.h"

//constructor of the class to get info about the exact position to start from
Camera::Camera(glm::vec3 position, unsigned int width, unsigned int height) {
	Camera::position = position;
	Camera::width = width;
	Camera::height = height;
}

//update the camera position and orientation in the graphics card
void Camera::matrix(float nPlane, float fPlane, Shader& shader, const char* uniform) {
	glm::mat4 view = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);

	view = glm::lookAt(position, position + orientation, up);

	projection = glm::perspective(glm::radians(fov), (float)width / height, nPlane, fPlane);

	glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform), 1, GL_FALSE, glm::value_ptr(projection * view));
}

//run the camera inputs to move around and see the world scene
void Camera::processCamInputs(GLFWwindow* window, float deltatime) {
    //process movement
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)   position += speed * orientation * deltatime;
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)   position -= speed * orientation * deltatime;
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)   position += speed * right * deltatime;
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)   position -= speed * right * deltatime;

    //process rotation
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
        if (firstClick) {
            glfwSetCursorPos(window, (float)width / 2, (float)height / 2);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            firstClick = false;
        }

        double mouseX;
        double mouseY;

        // receive the coordinates of the cursor
        glfwGetCursorPos(window, &mouseX, &mouseY);

        float rotX = sensitivity * (float)(mouseY - (height / 2)) / height;
        float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;

        glm::vec3 newOrientation = glm::rotate(orientation, glm::radians(-rotX), right);

        if (abs(glm::angle(newOrientation, up) - glm::radians(90.0f)) <= glm::radians(89.0f)) {
            orientation = newOrientation;
        }
        orientation = glm::rotate(orientation, glm::radians(-rotY), up);
        right = glm::normalize(glm::cross(orientation, up));
        glfwSetCursorPos(window, (width / 2), (height / 2));
    }
    else if (glfwGetKey(window, GLFW_KEY_R) == GLFW_RELEASE) {
        if (!firstClick) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            firstClick = true;
        }
    }
}

//process zooming in and out
void Camera::scrolling(float yoffset) {
    fov -= yoffset;
    if (fov > 120.0f) fov = 120.0f;
    if (fov < 5.0f)fov = 5.0f;
}

glm::vec3 Camera::currentPosition() {
    return position;
};

glm::vec3 Camera::frontDirection() {
    return orientation;
}