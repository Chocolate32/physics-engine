#include "Mesh.h"

//This is the Mesh's class constructor, it takes as input a vector of vertices, vector of indices and, a vector of textures
Mesh::Mesh(std::vector<Vertex>& vertices, std::vector<GLuint>& indices, std::vector<Texture>& textures) {
	Mesh::vertices = vertices;
	Mesh::indices = indices;
	Mesh::textures = textures;

	setupMesh();
}

//This functions is responsible for Drawing the mesh on the screen, it requires a shader program to use it and compile the GLSL code,
//and the camera to actually render what we see
void Mesh::Draw(Shader& shader, Camera& camera) {
	//Initiate the shader and vao for use
	shader.use();
	VAO.bind();

	//those to make a call for the uniforms in the GPU
	std::string prefix = "material.";
	unsigned int numDiffuse = 0;
	unsigned int numSpecular = 0;

	//loop through all the textures and apply them to the corresponding materials, interacting with the GLSL uniforms
	for (unsigned int i = 0; i < textures.size(); ++i) {
		std::string num;
		std::string type = textures[i].type;

		if (type == "diffuse") num = std::to_string(numDiffuse++);
		else if (type == "specular") num = std::to_string(numSpecular++);
		textures[i].texUnit(shader, (prefix + type + num).c_str(), i);
		textures[i].bind();
	}

	//glUniform3fv(shader.getUniformID("viewPos"), 1, glm::value_ptr(camera.currentPosition()));
	//camera.matrix(0.1f, 100.0f, shader, "camMatrix");

	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
}

//this function is responsible for setting up the mesh, and binding some data to the GPU's layout
void Mesh::setupMesh() {
	VAO.bind();

	VBO VBO(vertices);

	EBO EBO(indices);

	VAO.linkVBO(VBO, 0, 3, GL_FLOAT, sizeof(Vertex), (void*)0);
	VAO.linkVBO(VBO, 1, 3, GL_FLOAT, sizeof(Vertex), (void*)(3 * sizeof(float)));
	VAO.linkVBO(VBO, 2, 2, GL_FLOAT, sizeof(Vertex), (void*)(6 * sizeof(float)));
	VAO.unbind();
	VBO.unbind();
	EBO.unbind();
}