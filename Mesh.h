#ifndef MESH_H
#define MESH_H

//header files will always contain the protypes or the function that we need to use in the class, 
//this way we can keep our code clean and easy to read, maintain

#include <string>

#include "VAO.h"
#include "EBO.h"
#include "Camera.h"
#include "Texture.h"

class Mesh {
public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	VAO VAO;

	Mesh(std::vector<Vertex>& vertices, std::vector<GLuint>& indices, std::vector<Texture>& textures);
	void Draw(Shader& shader, Camera& camera);

private:
	void setupMesh();
};

#endif // !MESH_H