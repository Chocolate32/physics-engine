#version 330 core
out vec4 FragColor;

struct Material{
	sampler2D diffuse0;
	sampler2D specular0;
	float shininess;
};

struct DirLight{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutOff;
	float outterCutOff;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

#define nbr_of_spotlights 4

in vec3 fragPos;
in vec3 normal;
in vec2 texCoords;

uniform vec3 viewPos;
uniform Material material;
uniform DirLight dirLight;
uniform PointLight pointLight[nbr_of_spotlights];
uniform SpotLight spotLight;

vec3 ComputeDirectionalLights(DirLight light, vec3 norm, vec3 viewDir);
vec3 ComputePointLights(PointLight light, vec3 norm, vec3 fragPos, vec3 viewDir);
vec3 ComputeSpotLights(SpotLight light, vec3 norm, vec3 fragPos, vec3 viewDir);

void main()
{
	//properties
	vec3 norm = normalize(normal);
	vec3 viewDir = normalize(viewPos - fragPos);

	vec3 result = ComputeDirectionalLights(dirLight, norm, viewDir);
	for(int i = 0; i < nbr_of_spotlights; ++i)
		result += ComputePointLights(pointLight[i], norm, fragPos, viewDir);

	result += ComputeSpotLights(spotLight, norm, fragPos, viewDir);

	FragColor = vec4(result, 1.0);
}

//lights calculations 
vec3 ComputeDirectionalLights(DirLight light, vec3 norm, vec3 viewDir){
	vec3 lightDir = normalize(-light.direction);

	//diffuse shading
	float diff = max(dot(norm, lightDir), 0.0);

	//specular shading
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	//combine result
	vec3 ambient = light.ambient * vec3(texture(material.diffuse0, texCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse0, texCoords));
	vec3 specular = light.specular * spec * vec3(texture(material.specular0, texCoords));

	//result
	return (ambient + diffuse + specular);
}

vec3 ComputePointLights(PointLight light, vec3 norm, vec3 fragPos, vec3 viewDir){
	vec3 lightDir = normalize(light.position - fragPos);

	//diffuse shading
	float diff = max(dot(norm, lightDir), 0.0);

	//specular shading
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	//combine result
	vec3 specular = light.specular * spec * vec3(texture(material.specular0, texCoords));
	vec3 ambient = light.ambient * vec3(texture(material.diffuse0, texCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse0, texCoords));

	//attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	ambient *=attenuation;
	diffuse *=attenuation;
	specular *=attenuation;

	//result
	return (ambient + diffuse + specular);
}

vec3 ComputeSpotLights(SpotLight light, vec3 norm, vec3 fragPos, vec3 viewDir){
		vec3 lightDir = normalize(light.position - fragPos);
	//diffuse shading
	float diff = max(dot(norm, lightDir), 0.0);

	//specular shading
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	
	//spolight intensity(soft edges)
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = (light.cutOff - light.outterCutOff);
	float intensity = clamp((theta - light.outterCutOff)/epsilon, 0.0, 1.0);

	//attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	//combine
	vec3 specular = light.specular * spec * vec3(texture(material.specular0, texCoords));
	vec3 ambient = light.ambient * vec3(texture(material.diffuse0, texCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse0, texCoords));
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	//result
	return (ambient + diffuse + specular);
}