#include "VAO.h"

//constructor of the VAO will generate a vertex array object
VAO::VAO() 
{
	glGenVertexArrays(1, &ID);
}

//VAO is a holder of EBO and VBO this will make sending data to the GPU easier and better,
//sending data to the gpu requires some times so we should send as much as we can once sent
//at a time, this way we can increase the performance, 
void VAO::linkVBO(VBO& VBO, GLuint layout, GLuint nbComponents, GLenum type, GLsizeiptr stride, void* offset)
{
	VBO.bind();
	glVertexAttribPointer(layout, nbComponents, type, GL_FALSE, stride, offset);
	glEnableVertexAttribArray(layout);
	VBO.unbind();
}

//bind the data
void VAO::bind() 
{
	glBindVertexArray(ID);
}

//unbind the data
void VAO::unbind() 
{
	glBindVertexArray(0);
}

//destroy the object buffer
void VAO::destroy() 
{
	glDeleteVertexArrays(1, &ID);
}